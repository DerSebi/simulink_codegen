use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;
use crate::blocks::SimulinkDataType;

use std::collections::HashMap;

pub struct BlockConstant {
    value: String,
}

impl BlockTrait for BlockConstant {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec!["ZOrder", "Position", "ShowName", "DisableCoverage"];
        let accepted_parameters = vec!["Value", "OutDataTypeStr"];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            Some(&accepted_parameters),
            "Constant",
        );
        let mut the_value = match parameters.remove("Value") {
            None => String::new(),
            Some(v) => v,
        };
        let mut data_type = match parameters.get("OutDataTypeStr") {
            None => SimulinkDataType::Inherit,
            Some(v) => SimulinkDataType::inherit_from_value(v),
        };
        if data_type == SimulinkDataType::Inherit {
            data_type = SimulinkDataType::inherit_from_value(&the_value);
        }
        if the_value.is_empty() {
            the_value = data_type.get_default_value();
        }
        let output_data_types = vec![data_type];
        let constant = BlockConstant { value: the_value };
        BlockCreationResult {
            block: Box::new(constant),
            num_input_ports: 0,
            num_output_ports: 1,
            output_port_data_types: Some(output_data_types),
        }
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        let port_name = quote::format_ident!("{}", block.get_out_port_storage_location(1));
        let value = match block.out_ports[&1].data_type {
            SimulinkDataType::Double => {
                let value = self.value.parse::<f64>().unwrap();
                quote::quote! {
                    #value
                }
            }
            SimulinkDataType::Int32 => {
                let value = self.value.parse::<i32>().unwrap();
                quote::quote! {
                    #value
                }
            }
            SimulinkDataType::UInt32 => {
                let value = self.value.parse::<u32>().unwrap();
                quote::quote! {
                    #value
                }
            }
            _ => {
                panic!("Constant with unknown datatype");
            }
        };
        let code = quote::quote! {
            state.#port_name = #value;
        };
        Some(code.to_string())
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String> {
        Ok(true)
    }
}
