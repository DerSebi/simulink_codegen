pub mod constant;
pub mod delay;
pub mod display;
pub mod from;
pub mod goto;
pub mod ground;
pub mod logic;
pub mod port;
pub mod relational;
pub mod stop;
pub mod sum;
pub mod terminator;

use std::collections::HashMap;
use std::collections::HashSet;

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Sid {
    pub sid: u32,
    pub parent_sid: u32,
}

impl Sid {
    pub fn new(sid: u32, parent_sid: u32) -> Sid {
        Sid { sid, parent_sid }
    }

    pub fn parse(input: &str) -> Option<Sid> {
        if input.contains(':') {
            let (partent_sid_str, sid_str) = input.split_once(':')?;
            let parent_sid = partent_sid_str.parse().ok()?;
            let sid = sid_str.parse().ok()?;
            Some(Sid { parent_sid, sid })
        } else {
            let sid = input.parse().ok()?;
            Some(Sid { parent_sid: 0, sid })
        }
    }
}

pub struct BlockCreationResult {
    block: Box<dyn BlockTrait>,
    num_input_ports: u32,
    num_output_ports: u32,
    output_port_data_types: Option<Vec<SimulinkDataType>>,
}

pub trait BlockTrait {
    fn generate_state_entry_code(&self, _block: &Block) -> Option<String> {
        None
    }
    fn generate_state_init_code(&self, _block: &Block) -> Option<String> {
        None
    }
    fn generate_setup_code(&self, _block: &Block) -> Option<String> {
        None
    }
    fn generate_update_code(&self, _block: &Block) -> Option<String> {
        None
    }
    fn generate_outputs_code(&self, _block: &Block) -> Option<String> {
        None
    }
    fn generate_teardown_code(&self, _block: &Block) -> Option<String> {
        None
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String>;
    fn update_data_types(
        &mut self,
        in_ports: &HashMap<u32, crate::Port>,
        out_ports: &HashMap<u32, crate::Port>,
    ) -> (
        HashMap<u32, SimulinkDataType>,
        HashMap<u32, SimulinkDataType>,
    ) {
        let mut in_types = HashMap::<u32, SimulinkDataType>::new();
        let mut out_types = HashMap::<u32, SimulinkDataType>::new();
        for (index, port) in in_ports.iter() {
            in_types.insert(*index, port.data_type);
        }
        for (index, port) in out_ports.iter() {
            out_types.insert(*index, port.data_type);
        }
        (in_types, out_types)
    }

    fn new_block(parameters: HashMap<String, String>) -> BlockCreationResult
    where
        Self: Sized;
}

pub struct Block {
    rank: u32,
    sid: Sid,
    name: String,
    pub in_ports: HashMap<u32, crate::Port>,
    pub out_ports: HashMap<u32, crate::Port>,
    pub specialization: Box<dyn BlockTrait>,
    _path_in_model: Vec<String>,
}

impl Block {
    fn new<Type: BlockTrait>(
        name: String,
        sid: Sid,
        parameters: HashMap<String, String>,
        path_in_model: Vec<String>,
    ) -> Block {
        let result = Type::new_block(parameters);
        let mut in_ports = HashMap::new();
        for i in 0..result.num_input_ports {
            in_ports.insert(i + 1, crate::Port::new(SimulinkDataType::Inherit));
        }
        let mut out_ports = HashMap::new();
        let unwrapped_data_types = match result.output_port_data_types {
            Some(types) => types,
            None => vec![SimulinkDataType::Inherit],
        };
        for i in 0..result.num_output_ports as usize {
            let data_type = match i < unwrapped_data_types.len() {
                true => unwrapped_data_types[i],
                false => SimulinkDataType::Inherit,
            };
            out_ports.insert((i + 1) as u32, crate::Port::new(data_type));
        }
        Block {
            rank: u32::MAX,
            sid,
            name,
            in_ports,
            out_ports,
            specialization: result.block,
            _path_in_model: path_in_model,
        }
    }

    pub fn generate_ports_storage_entry_code(&self) -> Option<String> {
        if self.out_ports.is_empty() {
            return None;
        }
        let mut result = String::new();
        for (_index, port) in self.out_ports.iter() {
            let port_name = &port.storage_location;
            let port_data_type = port.data_type.as_rust_type_string();
            result += format!("\t{} : {},\n", port_name, port_data_type).as_str();
        }
        Some(result)
    }

    pub fn generate_ports_storage_init_code(&self) -> Option<String> {
        if self.out_ports.is_empty() {
            return None;
        }
        let mut result = String::new();
        for (_index, port) in self.out_ports.iter() {
            let port_name = &port.storage_location;
            let value = port.data_type.get_default_value();
            result += format!("\t\t\t{} : {},\n", port_name, value).as_str();
        }
        Some(result)
    }

    pub fn get_rank(&self) -> u32 {
        self.rank
    }

    pub fn set_rank(&mut self, new_rank: u32) {
        self.rank = new_rank;
    }

    pub fn get_name(&self) -> &String {
        &self.name
    }

    pub fn get_sid(&self) -> &Sid {
        &self.sid
    }

    pub fn get_in_port_storage_location(&self, port_number: u32) -> &String {
        &self.in_ports[&port_number].storage_location
    }

    pub fn get_out_port_storage_location(&self, port_number: u32) -> &String {
        &self.out_ports[&port_number].storage_location
    }

    pub fn update_data_types(&mut self) {
        let (in_types, out_types) = self
            .specialization
            .update_data_types(&self.in_ports, &self.out_ports);
        for (i, t) in in_types.iter() {
            if self.in_ports[i].data_type != *t {
                self.in_ports.get_mut(i).unwrap().data_type = *t;
            }
        }
        for (i, t) in out_types.iter() {
            if self.out_ports[i].data_type != *t {
                self.out_ports.get_mut(i).unwrap().data_type = *t;
            }
        }
    }
}

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum SimulinkDataType {
    Unknown,
    Double,
    Int32,
    UInt32,
    Boolean,
    Inherit,
}

impl SimulinkDataType {
    fn from_string(input: &str) -> SimulinkDataType {
        match input {
            "double" => SimulinkDataType::Double,
            "int32" => SimulinkDataType::Int32,
            "uint32" => SimulinkDataType::UInt32,
            "boolean" => SimulinkDataType::Boolean,
            "Inherit" | "Inherit: Inherit via internal rule" => SimulinkDataType::Inherit,
            v => {
                log::error!("Unknown data type {}", v);
                SimulinkDataType::Unknown
            }
        }
    }

    pub fn as_rust_type_string(&self) -> String {
        match self {
            SimulinkDataType::Unknown => "unknown".to_string(),
            SimulinkDataType::Double => "f64".to_string(),
            SimulinkDataType::Int32 => "i32".to_string(),
            SimulinkDataType::UInt32 => "u32".to_string(),
            SimulinkDataType::Boolean => "bool".to_string(),
            SimulinkDataType::Inherit => "inherited".to_string(),
        }
    }

    pub fn is_integral(&self) -> bool {
        matches!(self, SimulinkDataType::Int32 | SimulinkDataType::UInt32)
    }

    pub fn is_floating_point(&self) -> bool {
        matches!(self, SimulinkDataType::Double)
    }

    fn is_defined(&self) -> bool {
        !matches!(self, SimulinkDataType::Inherit | SimulinkDataType::Unknown)
    }
    
    fn can_represent_zero(&self) -> bool {
        matches!(self, SimulinkDataType::Boolean | SimulinkDataType::Int32 | SimulinkDataType::Double | Self::UInt32)
    }

    fn inherit_from_value(value: &str) -> SimulinkDataType {
        if value.is_empty() {
            return SimulinkDataType::Double;
        }
        if value.contains('.') {
            return SimulinkDataType::Double;
        }
        if value == "true" || value == "false" {
            return SimulinkDataType::Boolean;
        }
        SimulinkDataType::Int32
    }

    fn get_default_value(&self) -> String {
        match self {
            SimulinkDataType::Unknown => "1".to_string(),
            SimulinkDataType::Double => "1.0".to_string(),
            SimulinkDataType::Int32 => "1".to_string(),
            SimulinkDataType::UInt32 => "1".to_string(),
            SimulinkDataType::Boolean => "false".to_string(),
            SimulinkDataType::Inherit => "1".to_string(),
        }
    }
}

pub fn parameter_filter(
    parameters: &mut HashMap<String, String>,
    ignored: Option<&Vec<&str>>,
    accepted: Option<&Vec<&str>>,
    block_type: &str,
) {
    if let Some(ignored) = ignored {
        parameters.retain(|k, _| !ignored.contains(&k.as_str()));
    }
    if let Some(accepted) = accepted {
        for (key, value) in parameters.iter() {
            if !accepted.contains(&key.as_str()) {
                log::warn!(
                    "Unsupported parameter for <{}>: {} : {}",
                    block_type,
                    key,
                    value
                );
            }
        }
        parameters.retain(|k, _| accepted.contains(&k.as_str()));
    } else {
        for (key, value) in parameters.iter() {
            log::warn!(
                "Unsupported parameter for <{}>: {} : {}",
                block_type,
                key,
                value
            );
        }
    }
}

pub fn block_factory(
    name: String,
    sid: Sid,
    block_type: String,
    parameters: HashMap<String, String>,
    path_in_model: Vec<String>,
    unsupported_blocks: &mut HashSet<String>,
) -> Option<Block> {
    match block_type.as_str() {
        "Constant" => Some(Block::new::<constant::BlockConstant>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Display" => Some(Block::new::<display::BlockDisplay>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Sum" => Some(Block::new::<sum::BlockSum>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Delay" => Some(Block::new::<delay::BlockDelay>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Terminator" => Some(Block::new::<terminator::BlockTerminator>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Inport" => Some(Block::new::<port::BlockPort>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Outport" => Some(Block::new::<port::BlockPort>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "RelationalOperator" => Some(Block::new::<relational::BlockRelationalOperator>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Stop" => Some(Block::new::<stop::BlockStop>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Goto" => Some(Block::new::<goto::BlockGoto>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "From" => Some(Block::new::<from::BlockFrom>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Ground" => Some(Block::new::<ground::BlockGround>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        "Logic" => Some(Block::new::<logic::BlockLogic>(
            name,
            sid,
            parameters,
            path_in_model,
        )),
        v => {
            unsupported_blocks.insert(v.to_string());
            None
        }
    }
}
