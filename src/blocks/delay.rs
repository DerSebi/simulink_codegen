use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;
use crate::blocks::SimulinkDataType;

use std::collections::HashMap;

pub struct BlockDelay {
    _use_circular_buffer: bool,
    delay_length: usize,
    data_type: SimulinkDataType,
}

impl BlockTrait for BlockDelay {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec!["ZOrder", "Position", "ShowName"];
        //Ports and InputPortMap currently unused
        let accepted_parameters = vec!["UseCircularBuffer", "DelayLength", "Ports", "InputPortMap"];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            Some(&accepted_parameters),
            "Delay",
        );
        let mut use_circular_buffer = match parameters.get("UseCircularBuffer") {
            None => false,
            Some(v) => v != "off",
        };
        let delay_length = match parameters.get("DelayLength") {
            None => 2,
            Some(v) => v.parse::<usize>().unwrap(),
        };

        if !use_circular_buffer {
            log::info!("Circular buffer was turned off for Delay block - turning on");
            use_circular_buffer = true;
        }
        let data_type = SimulinkDataType::Inherit;
        let delay = BlockDelay {
            _use_circular_buffer: use_circular_buffer,
            delay_length,
            data_type,
        };
        let output_data_types = vec![data_type];
        BlockCreationResult {
            block: Box::new(delay),
            num_input_ports: 1,
            num_output_ports: 1,
            output_port_data_types: Some(output_data_types),
        }
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String> {
        Ok(true)
    }

    fn update_data_types(
        &mut self,
        in_ports: &HashMap<u32, crate::Port>,
        out_ports: &HashMap<u32, crate::Port>,
    ) -> (
        HashMap<u32, SimulinkDataType>,
        HashMap<u32, SimulinkDataType>,
    ) {
        let in_types = HashMap::from([(1, in_ports[&1].data_type)]);
        let out_types;

        match in_ports[&1].data_type {
            SimulinkDataType::Unknown | SimulinkDataType::Inherit => {
                out_types = HashMap::from([(1, out_ports[&1].data_type)]);
            }
            v => {
                out_types = HashMap::from([(1, v)]);
                self.data_type = v;
            }
        }

        (in_types, out_types)
    }

    fn generate_state_entry_code(&self, block: &Block) -> Option<String> {
        let write_index_var =
            quote::format_ident!("{}_write_index", block.get_name().to_lowercase());
        let read_index_var = quote::format_ident!("{}_read_index", block.get_name().to_lowercase());
        let buffer_var = quote::format_ident!("{}_buffer", block.get_name().to_lowercase());
        let data_type = quote::format_ident!("{}", self.data_type.as_rust_type_string());
        let buffer_size = self.delay_length;
        let code = quote::quote! {
            #write_index_var : usize,
            #read_index_var : usize,
            #buffer_var : [#data_type;#buffer_size],
        };
        Some(code.to_string())
    }

    fn generate_state_init_code(&self, block: &Block) -> Option<String> {
        let write_index_var =
            quote::format_ident!("{}_write_index", block.get_name().to_lowercase());
        let read_index_var = quote::format_ident!("{}_read_index", block.get_name().to_lowercase());
        let buffer_var = quote::format_ident!("{}_buffer", block.get_name().to_lowercase());
        let buffer_size = self.delay_length;
        let default_value = self.data_type.get_default_value().parse::<f64>().unwrap();
        let code = quote::quote! {
            #write_index_var : 0,
            #read_index_var : 0,
            #buffer_var : [#default_value;#buffer_size],
        };
        Some(code.to_string())
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        let in_port_name = quote::format_ident!("{}", block.get_in_port_storage_location(1));
        let out_port_name = quote::format_ident!("{}", block.get_out_port_storage_location(1));
        let write_index_var =
            quote::format_ident!("{}_write_index", block.get_name().to_lowercase());
        let read_index_var = quote::format_ident!("{}_read_index", block.get_name().to_lowercase());
        let buffer_var = quote::format_ident!("{}_buffer", block.get_name().to_lowercase());
        let buffer_size = self.delay_length;
        let code = quote::quote! {
            let value = state.#buffer_var[state.#read_index_var];
            state.#read_index_var = (state.#read_index_var + 1) % #buffer_size;
            state.#buffer_var[state.#write_index_var] = state.#in_port_name;
            state.#write_index_var = (state.#write_index_var + 1) % #buffer_size;
            state.#out_port_name = value;
        };
        Some(code.to_string())
    }
}
