use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;
use crate::blocks::SimulinkDataType;

use std::collections::HashMap;

pub struct BlockSum {
    inputs: Vec<char>,
    _in_ports: u32,
    _out_ports: u32,
    saturate_on_int_overflow: bool,
    input_same_dt: bool,
}

impl BlockTrait for BlockSum {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec!["ZOrder", "Position", "ShowName", "IconShape"];
        let accepted_parameters = vec![
            "OutDataTypeStr",
            "Inputs",
            "Ports",
            "SaturateOnIntegerOverflow",
            "InputSameDT",
        ];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            Some(&accepted_parameters),
            "Sum",
        );

        let output_data_types = match parameters.get("OutDataTypeStr") {
            None => vec![SimulinkDataType::Inherit],
            Some(v) => vec![SimulinkDataType::from_string(v)],
        };
        let inputs = match parameters.remove("Inputs") {
            None => String::new(),
            Some(mut v) => {
                v.retain(|c| c != '|');
                v
            }
        };
        let (in_ports, out_ports) = match parameters.remove("Ports") {
            None => (2, 1),
            Some(mut v) => {
                v.retain(|c| c != '[' && c != ']');
                let (in_part, out_part) = v.split_once(',').unwrap();
                let in_ports = in_part.trim().parse::<u32>().unwrap();
                let out_ports = out_part.trim().parse::<u32>().unwrap();
                (in_ports, out_ports)
            }
        };
        let saturate_on_int_overflow = match parameters.get("SaturateOnIntegerOverflow") {
            None => false,
            Some(v) => v != "off",
        };
        let input_same_dt = match parameters.get("InputSameDT") {
            None => false,
            Some(v) => v != "off",
        };
        let sum = BlockSum {
            inputs: inputs.chars().collect(),
            _in_ports: in_ports,
            _out_ports: out_ports,
            saturate_on_int_overflow,
            input_same_dt,
        };
        BlockCreationResult {
            block: Box::new(sum),
            num_input_ports: in_ports,
            num_output_ports: out_ports,
            output_port_data_types: Some(output_data_types),
        }
    }

    fn check_integrity(&self, block: &Block) -> Result<bool, String> {
        if self.input_same_dt {
            let reference_datatype = block.in_ports[&1].data_type;
            for (_index, port) in block.in_ports.iter() {
                if port.data_type != reference_datatype {
                    return Err(format!(
                        "Inconsistent input data types for block {}",
                        block.get_name()
                    ));
                }
            }
        }
        if self.inputs.len() != block.in_ports.len() {
            return Err(format!(
                "Input symbols don't match the port count for Sum block {}",
                block.get_name()
            ));
        }
        Ok(true)
    }

    fn update_data_types(
        &mut self,
        in_ports: &HashMap<u32, crate::Port>,
        out_ports: &HashMap<u32, crate::Port>,
    ) -> (
        HashMap<u32, SimulinkDataType>,
        HashMap<u32, SimulinkDataType>,
    ) {
        let mut in_types =
            HashMap::from([(1, in_ports[&1].data_type), (2, in_ports[&2].data_type)]);
        let mut out_types = HashMap::from([(1, out_ports[&1].data_type)]);

        let mut data_type = SimulinkDataType::Unknown;
        if out_types[&1].is_defined() {
            data_type = out_types[&1];
        }

        for port in in_ports.values() {
            if port.data_type.is_defined() && data_type == SimulinkDataType::Unknown {
                data_type = port.data_type;
            }
        }
        for (_index, in_type) in in_types.iter_mut() {
            if *in_type == SimulinkDataType::Inherit {
                *in_type = data_type;
            }
        }
        if out_types[&1] == SimulinkDataType::Inherit {
            out_types.insert(1, data_type);
        }

        (in_types, out_types)
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        let mut port_names = vec![];
        let mut port_data_types = vec![];
        for (index, _port) in block.in_ports.iter() {
            port_names.push(quote::format_ident!(
                "{}",
                block.get_in_port_storage_location(*index)
            ));
            port_data_types.push(block.in_ports[index].data_type);
        }
        let out_port_name = quote::format_ident!("{}", block.get_out_port_storage_location(1));
        let out_data_type =
            quote::format_ident!("{}", block.out_ports[&1].data_type.as_rust_type_string());
        let is_integral = block.out_ports[&1].data_type.is_integral();

        let mut code = quote::quote! { state.#out_port_name = };
        for (index, c) in self.inputs.iter().enumerate() {
            let token = &port_names[index];
            let operand = match block.out_ports[&1].data_type != port_data_types[index] {
                true => {
                    quote::quote! {#out_data_type::from(state.#token)}
                }
                false => {
                    quote::quote! {state.#token}
                }
            };
            if *c == '-' {
                if index == 0 {
                    if block.out_ports[&1].data_type == SimulinkDataType::Double {
                        code = quote::quote! { #code #out_data_type::from(0.0) };
                    } else {
                        code = quote::quote! { #code 0.0 };
                    }
                }
                if is_integral && self.saturate_on_int_overflow {
                    code = quote::quote! {#code.saturating_sub(#operand)};
                } else {
                    code = quote::quote! {#code - #operand};
                }
            } else if index == 0 {
                code = quote::quote! {#code #operand};
            } else if is_integral && self.saturate_on_int_overflow {
                code = quote::quote! {#code.saturating_add(#operand)};
            } else {
                code = quote::quote! {#code + #operand};
            }
        }
        code = quote::quote! { #code ; };
        Some(code.to_string())
    }
}
