use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;

use std::collections::HashMap;

pub struct BlockDisplay {
    decimation: u32,
    _ports: String,
}

impl BlockTrait for BlockDisplay {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec!["ZOrder", "Position"];
        let accepted_parameters = vec!["Ports", "Decimation"];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            Some(&accepted_parameters),
            "Display",
        );

        let ports = match parameters.remove("Ports") {
            None => "[1]".to_string(),
            Some(v) => {
                if v != "[1]" {
                    log::warn!("Display only supports scalar input data");
                }
                v
            }
        };
        let decimation = match parameters.get("Decimation") {
            None => 1,
            Some(v) => v.parse::<u32>().unwrap(),
        };

        let display = BlockDisplay {
            decimation,
            _ports: ports,
        };
        BlockCreationResult {
            block: Box::new(display),
            num_input_ports: 1,
            num_output_ports: 0,
            output_port_data_types: None,
        }
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String> {
        Ok(true)
    }

    fn generate_state_entry_code(&self, block: &Block) -> Option<String> {
        let variable_name = format!("{}_decimation", block.get_name()).to_lowercase();
        let code = format!("\t{} : u32,\n", variable_name);
        Some(code)
    }

    fn generate_state_init_code(&self, block: &Block) -> Option<String> {
        let variable_name = format!("{}_decimation", block.get_name()).to_lowercase();
        let code = format!("\t\t\t{} : {},\n", variable_name, 0);
        Some(code)
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        let port_name = quote::format_ident!("{}", block.get_in_port_storage_location(1));
        let cycle_count_name =
            quote::format_ident!("{}_decimation", block.get_name().to_lowercase());
        let decimation = self.decimation;
        let message_string = format!("Display {} : {{}}", block.get_name());

        let code = quote::quote! {
            if state.#cycle_count_name % #decimation == 0 {
                let value = state.#port_name;
                println!(#message_string, value);
            }
            state.#cycle_count_name += 1;
        };
        Some(code.to_string())
    }
}
