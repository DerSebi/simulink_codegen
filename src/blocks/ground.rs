use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;
use crate::blocks::SimulinkDataType;

use std::collections::HashMap;

pub struct BlockGround {}

impl BlockTrait for BlockGround {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec![
            "ZOrder",
            "Position",
            "IconDisplay",
            "BackgroundColor",
            "ShowName",
        ];
        let accepted_parameters = vec![];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            Some(&accepted_parameters),
            "Ground",
        );

        let output_data_types = vec![SimulinkDataType::Inherit];

        let ground = BlockGround {};
        BlockCreationResult {
            block: Box::new(ground),
            num_input_ports: 0,
            num_output_ports: 1,
            output_port_data_types: Some(output_data_types),
        }
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String> {
        Ok(true)
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        let out_port_name = quote::format_ident!("{}", block.get_out_port_storage_location(1));

        let code = quote::quote! { state.#out_port_name = 0; };
        Some(code.to_string())
    }
}
