use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;
use crate::blocks::SimulinkDataType;

use std::collections::HashMap;

pub struct BlockGoto {
    _goto_tag: String,
}

impl BlockTrait for BlockGoto {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec![
            "ZOrder",
            "Position",
            "IconDisplay",
            "BackgroundColor",
            "ShowName",
        ];
        let accepted_parameters = vec!["GotoTag"];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            Some(&accepted_parameters),
            "Goto",
        );

        let goto_tag = parameters.remove("GotoTag").unwrap();
        let output_data_types = vec![SimulinkDataType::Inherit];

        let goto = BlockGoto {
            _goto_tag: goto_tag,
        };
        BlockCreationResult {
            block: Box::new(goto),
            num_input_ports: 1,
            num_output_ports: 1,
            output_port_data_types: Some(output_data_types),
        }
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String> {
        Ok(true)
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        let out_port_name = quote::format_ident!("{}", block.get_out_port_storage_location(1));
        let in_port_name = quote::format_ident!("{}", block.get_in_port_storage_location(1));

        let code = quote::quote! { state.#out_port_name = state.#in_port_name; };
        Some(code.to_string())
    }
}
