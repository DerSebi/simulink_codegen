use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;
use crate::blocks::SimulinkDataType;

use std::collections::HashMap;

pub struct BlockRelationalOperator {
    operator: String,
    number_of_inputs: u8,
    _data_type: SimulinkDataType,
}

impl BlockTrait for BlockRelationalOperator {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec!["ZOrder", "Position"];
        let accepted_parameters = vec!["Operator", "Ports", "OutDataTypeStr"];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            Some(&accepted_parameters),
            "RelationalOperator",
        );
        let operator = match parameters.remove("Operator") {
            None => String::from("<="),
            Some(v) => v,
        };
        let data_type = match parameters.get("OutDataTypeStr") {
            None => SimulinkDataType::Boolean,
            Some(v) => SimulinkDataType::from_string(v),
        };
        let number_of_inputs = match parameters.remove("Ports") {
            None => 2,
            Some(v) => match v.as_str() {
                "[2, 1]" => 2,
                "[1, 1]" => 1,
                _ => 0,
            },
        };

        let output_data_types = vec![data_type];
        let relational_operator = BlockRelationalOperator {
            operator,
            number_of_inputs,
            _data_type: data_type,
        };
        BlockCreationResult {
            block: Box::new(relational_operator),
            num_input_ports: u32::from(number_of_inputs),
            num_output_ports: 1,
            output_port_data_types: Some(output_data_types),
        }
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        let in1_port_name = quote::format_ident!("{}", block.get_in_port_storage_location(1));
        let out_port_name = quote::format_ident!("{}", block.get_out_port_storage_location(1));
        let operator = quote::format_ident!("{}", self.operator);
        if self.number_of_inputs == 1 {
            match self.operator.as_str() {
                "isInf" => {
                    let code;
                    if block.in_ports[&1].data_type.is_floating_point() {
                        code = quote::quote! {
                            state.#out_port_name = state.#in1_port_name.is_infinite();
                        };
                    } else {
                        code = quote::quote! {
                            state.#out_port_name = false;
                        };
                    }
                    return Some(code.to_string());
                }
                "isNaN" => {
                    let code;
                    if block.in_ports[&1].data_type.is_floating_point() {
                        code = quote::quote! {
                            state.#out_port_name = state.#in1_port_name.is_nan();
                        };
                    } else {
                        code = quote::quote! {
                            state.#out_port_name = false;
                        };
                    }
                    return Some(code.to_string());
                }
                "ifFinite" => {
                    let code;
                    if block.in_ports[&1].data_type.is_floating_point() {
                        code = quote::quote! {
                            state.#out_port_name = state.#in1_port_name.is_finite();
                        };
                    } else {
                        code = quote::quote! {
                            state.#out_port_name = true;
                        };
                    }
                    return Some(code.to_string());
                }
                _ => {}
            }
        } else if self.number_of_inputs == 2 {
            let in2_port_name = quote::format_ident!("{}", block.get_in_port_storage_location(2));
            let code = quote::quote! {
                state.#out_port_name = state.#in1_port_name #operator state.#in2_port_name;
            };
            return Some(code.to_string());
        }
        None
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String> {
        match self.operator.as_str() {
            "==" | "~=" | "<" | "<=" | ">=" | ">" => {
                if self.number_of_inputs != 2 {
                    return Err(format!(
                        "Operator {} used with single port config which is unsupported",
                        self.operator
                    ));
                }
            }
            "isInf" | "isNaN" | "isFinite" => {
                if self.number_of_inputs != 1 {
                    return Err(format!(
                        "Operator {} used with double port config which is unsupported",
                        self.operator
                    ));
                }
            }
            v => return Err(format!("Unsupported relational operator: {}", v)),
        }
        if self.number_of_inputs != 1 && self.number_of_inputs != 2 {
            return Err("Unsupported number of input ports".to_string());
        }
        Ok(true)
    }

    fn update_data_types(
        &mut self,
        in_ports: &HashMap<u32, crate::Port>,
        out_ports: &HashMap<u32, crate::Port>,
    ) -> (
        HashMap<u32, SimulinkDataType>,
        HashMap<u32, SimulinkDataType>,
    ) {
        let mut in_types = HashMap::new();
        let out_types = HashMap::from([(1, out_ports[&1].data_type)]);
        for (key, port) in in_ports {
            in_types.insert(*key, port.data_type);
        }
        if self.number_of_inputs == 1 && !in_types[&1].is_defined() {
            in_types.insert(1, SimulinkDataType::Double);
        }
        if self.number_of_inputs == 2 {
            if !in_types[&1].is_defined() && !in_types[&2].is_defined() {
                in_types.insert(1, SimulinkDataType::Double);
                in_types.insert(2, SimulinkDataType::Double);
            } else if !in_types[&1].is_defined() {
                in_types.insert(1, in_types[&2]);
            } else if !in_types[&2].is_defined() {
                in_types.insert(2, in_types[&1]);
            }
        }
        (in_types, out_types)
    }
}
