use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;

use std::collections::HashMap;

pub struct BlockStop {}

impl BlockTrait for BlockStop {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec!["ZOrder", "Position", "BackgroundColor", "ShowName"];
        crate::blocks::parameter_filter(&mut parameters, Some(&ignored_parameters), None, "Stop");
        let stop = BlockStop {};
        BlockCreationResult {
            block: Box::new(stop),
            num_input_ports: 1,
            num_output_ports: 0,
            output_port_data_types: None,
        }
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String> {
        Ok(true)
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        let in_port_name = quote::format_ident!("{}", block.get_in_port_storage_location(1));

        let code = quote::quote! { if state.#in_port_name != 0 { state.running = false; } };
        Some(code.to_string())
    }
}
