use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;
use crate::blocks::SimulinkDataType;
use lazy_static::lazy_static;
use regex::Regex;

use std::collections::HashMap;

pub struct BlockLogic {
    operator: String,
    number_of_inputs: u8,
    data_type: SimulinkDataType,
    all_ports_same_dt: bool,
}

impl BlockTrait for BlockLogic {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec!["ZOrder", "Position", "ShowName"];
        let accepted_parameters = vec!["Operator", "Ports", "OutDataTypeStr", "AllPortsSameDT"];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            Some(&accepted_parameters),
            "Logic",
        );
        let operator = match parameters.remove("Operator") {
            None => String::from("<="),
            Some(v) => v,
        };
        let all_ports_same_dt = match parameters.remove("AllPortsSameDT") {
            None => false,
            Some(v) => v != "off",
        };
        let data_type = match parameters.get("OutDataTypeStr") {
            None => SimulinkDataType::Boolean,
            Some(v) => SimulinkDataType::from_string(v),
        };
        let number_of_inputs = match parameters.remove("Ports") {
            None => 2,
            Some(v) => {
                lazy_static! {
                    static ref RE: Regex = Regex::new(r"^[(\d+)\s*,\s*(\d+)]$").unwrap();
                }
                let captures = RE.captures(v.as_str()).unwrap();
                captures[1].parse::<u8>().unwrap()
            }
        };

        let output_data_types = vec![data_type];
        let logic = BlockLogic {
            operator,
            number_of_inputs,
            data_type,
            all_ports_same_dt,
        };
        BlockCreationResult {
            block: Box::new(logic),
            num_input_ports: u32::from(number_of_inputs),
            num_output_ports: 1,
            output_port_data_types: Some(output_data_types),
        }
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        //FIXME: implement logical operator
        let out_port_name = quote::format_ident!("{}", block.get_out_port_storage_location(1));
        let mut in_ports = HashMap::new();
        for key in block.in_ports.keys() {
            in_ports.insert(
                key,
                quote::format_ident!("{}", block.get_in_port_storage_location(*key)),
            );
        }
        let operator = quote::format_ident!("{}", self.operator);
        let code = quote::quote! { state.#out_port_name = };
        match self.operator.as_str() {
            "AND" => {
                //true if all inputs are true
            }
            "OR" => {
                //true if at least one input is true
            }
            "NAND" => {
                //true if at least one input is false
            }
            "NOR" => {
                //true if no input is true
            }
            "XOR" => {
                //true if an odd number of inputs are true
            }
            "NXOR" => {
                //true if an even number of inputs are true
            }
            "NOT" => {
                //true if all inputs are false
            }
            v => {
                log::error!("Unsupported logical operator {}", v)
            }
        }
        None
    }

    fn check_integrity(&self, block: &Block) -> Result<bool, String> {
        match self.operator.as_str() {
            "AND" | "OR" | "NAND" | "NOR" | "XOR" | "NXOR" => {}
            "NOT" => {
                if self.number_of_inputs != 1 {
                    return Err("NOT block only supports one input port".to_string());
                }
            }
            v => return Err(format!("Unsupported logical operator: {}", v)),
        }

        if self.all_ports_same_dt {
            let mut data_type = None;
            for port in block.in_ports.values() {
                if data_type.is_none() {
                    data_type = Some(port.data_type);
                } else if data_type.unwrap() != port.data_type {
                    return Err(format!(
                        "Port data types need to have the same type: {} vs {}",
                        data_type.unwrap().as_rust_type_string(),
                        port.data_type.as_rust_type_string()
                    ));
                }
            }
            if block.out_ports[&1].data_type != data_type.unwrap() {
                return Err(format!(
                    "Port data types need to have the same type: {} vs {}",
                    data_type.unwrap().as_rust_type_string(),
                    block.out_ports[&1].data_type.as_rust_type_string()
                ));
            }
        }
        if !self.data_type.can_represent_zero() {
            return Err(format!(
                "Output data type {} can not represent zero exactly",
                self.data_type.as_rust_type_string()
            ));
        }
        Ok(true)
    }

    fn update_data_types(
        &mut self,
        in_ports: &HashMap<u32, crate::Port>,
        out_ports: &HashMap<u32, crate::Port>,
    ) -> (
        HashMap<u32, SimulinkDataType>,
        HashMap<u32, SimulinkDataType>,
    ) {
        //FIXME: implement logical operator: replace all undefined data types with defined ones
        let mut in_types = HashMap::new();
        let out_types = HashMap::from([(1, out_ports[&1].data_type)]);
        for (key, port) in in_ports {
            in_types.insert(*key, port.data_type);
        }
        if self.number_of_inputs == 1 && !in_types[&1].is_defined() {
            in_types.insert(1, SimulinkDataType::Double);
        }
        if self.number_of_inputs == 2 {
            if !in_types[&1].is_defined() && !in_types[&2].is_defined() {
                in_types.insert(1, SimulinkDataType::Double);
                in_types.insert(2, SimulinkDataType::Double);
            } else if !in_types[&1].is_defined() {
                in_types.insert(1, in_types[&2]);
            } else if !in_types[&2].is_defined() {
                in_types.insert(2, in_types[&1]);
            }
        }
        (in_types, out_types)
    }
}
