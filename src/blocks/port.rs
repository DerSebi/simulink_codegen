use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;
use crate::blocks::SimulinkDataType;

use std::collections::HashMap;

#[derive(Debug)]
enum PortDirection {
    In,
    Out,
}

pub struct BlockPort {
    _direction: PortDirection,
    _port: u32,
}

impl BlockTrait for BlockPort {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec!["ZOrder", "Position", "IconDisplay", "BackgroundColor"];
        let accepted_parameters = vec!["Port", "Direction", "OutDataTypeStr", "PortDimensions"];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            Some(&accepted_parameters),
            "Port",
        );

        let port = match parameters.get("Port") {
            Some(v) => v.parse::<u32>().unwrap(),
            None => 1,
        };
        let _port_dimensions = match parameters.remove("PortDimensions") {
            None => 1,
            Some(v) => {
                let dimensions: u32 = v.parse().unwrap();
                if dimensions != 1 {
                    panic!("Unsupported port dimensions: {}", &dimensions);
                }
                dimensions
            }
        };
        let direction = match parameters.remove("Direction") {
            None => {
                panic!("Unknown port direction")
            }
            Some(v) => {
                if v == "In" {
                    PortDirection::In
                } else if v == "Out" {
                    PortDirection::Out
                } else {
                    panic!("Unknown port direction");
                }
            }
        };
        let output_data_types = match parameters.get("OutDataTypeStr") {
            None => vec![SimulinkDataType::Inherit],
            Some(v) => vec![SimulinkDataType::from_string(v)],
        };
        let port = BlockPort {
            _port: port,
            _direction: direction,
        };
        BlockCreationResult {
            block: Box::new(port),
            num_input_ports: 1,
            num_output_ports: 1,
            output_port_data_types: Some(output_data_types),
        }
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String> {
        Ok(true)
    }

    fn update_data_types(
        &mut self,
        in_ports: &HashMap<u32, crate::Port>,
        out_ports: &HashMap<u32, crate::Port>,
    ) -> (
        HashMap<u32, SimulinkDataType>,
        HashMap<u32, SimulinkDataType>,
    ) {
        let mut in_types = HashMap::from([(1, in_ports[&1].data_type)]);
        let mut out_types = HashMap::from([(1, out_ports[&1].data_type)]);
        if in_types[&1].is_defined() && !out_types[&1].is_defined() {
            out_types.insert(1, in_types[&1]);
        }
        if !in_types[&1].is_defined() && out_types[&1].is_defined() {
            in_types.insert(1, out_types[&1]);
        }
        (in_types, out_types)
    }

    fn generate_outputs_code(&self, block: &Block) -> Option<String> {
        let out_port_name = quote::format_ident!("{}", block.get_out_port_storage_location(1));
        let in_port_name = quote::format_ident!("{}", block.get_in_port_storage_location(1));

        let code = quote::quote! { state.#out_port_name = state.#in_port_name; };
        Some(code.to_string())
    }
}
