use crate::blocks::Block;
use crate::blocks::BlockCreationResult;
use crate::blocks::BlockTrait;
use crate::blocks::SimulinkDataType;

use std::collections::HashMap;

pub struct BlockTerminator {}

impl BlockTrait for BlockTerminator {
    fn new_block(mut parameters: HashMap<String, String>) -> BlockCreationResult {
        let ignored_parameters = vec!["ZOrder", "Position", "ShowName"];
        crate::blocks::parameter_filter(
            &mut parameters,
            Some(&ignored_parameters),
            None,
            "Terminator",
        );
        let output_data_types = vec![SimulinkDataType::Inherit];
        let terminator = BlockTerminator {};
        BlockCreationResult {
            block: Box::new(terminator),
            num_input_ports: 1,
            num_output_ports: 0,
            output_port_data_types: Some(output_data_types),
        }
    }

    fn check_integrity(&self, _block: &Block) -> Result<bool, String> {
        Ok(true)
    }
}
