use crate::blocks::Block;
use crate::blocks::SimulinkDataType;
use crate::Line;
use std::collections::HashMap;
use std::io::BufRead;
use std::io::Write;

pub fn order_blocks(
    lines: Vec<Line>,
    mut blocks: HashMap<crate::blocks::Sid, Block>,
) -> Result<Vec<Block>, String> {
    let mut result = vec![];
    let mut work_set = vec![];
    log::info!(
        "Number of blocks: {} - Number of lines: {}",
        blocks.len(),
        lines.len()
    );
    log::info!("Filling out port information");
    // fill out port info
    for line in lines.iter() {
        let src_name = &blocks[&line.src.sid].get_name();
        let storage_location = format!(
            "{}_{}_port_{}",
            src_name, line.src.sid.sid, line.src.port_number
        )
        .to_lowercase();
        let src_peers = vec![crate::PortConnection::from_line(
            line.src.sid.clone(),
            line.src.port_number,
        )];
        let mut dst_peers = vec![];
        for dst in line.dst.iter() {
            dst_peers.push(crate::PortConnection::from_line(
                dst.sid.clone(),
                dst.port_number,
            ));
        }

        let out_port = blocks
            .get_mut(&line.src.sid)
            .unwrap()
            .out_ports
            .get_mut(&(line.src.port_number))
            .unwrap();
        out_port.peers = Some(dst_peers);
        out_port.storage_location = storage_location.clone();

        for dst in line.dst.iter() {
            let in_port = blocks
                .get_mut(&dst.sid)
                .unwrap()
                .in_ports
                .get_mut(&(dst.port_number))
                .unwrap();
            in_port.peers = Some(src_peers.clone());
            in_port.storage_location = storage_location.clone();
        }
    }

    log::info!("Detecting unconnected ports");
    let mut error_detected = false;
    //all blocks with no input port start at rank 0
    for (sid, block) in blocks.iter_mut() {
        //determine rank 0 nodes
        if block.in_ports.is_empty() {
            work_set.push(sid.clone());
            block.set_rank(0);
        }
        for (index, in_port) in block.in_ports.iter() {
            if in_port.peers.is_none() {
                error_detected = true;
                log::error!(
                    "Unconnected input port {} of block {} with sid {}",
                    index,
                    block.get_name(),
                    sid.sid
                );
            }
        }
        for (index, out_port) in block.out_ports.iter() {
            if out_port.peers.is_none() {
                error_detected = true;
                log::error!(
                    "Unconnected output port {} of block {} with sid {}",
                    index,
                    block.get_name(),
                    sid.sid
                );
            }
        }
    }

    if error_detected {
        return Err("Check failed".to_string());
    }

    // calculate rank
    log::info!("Detecting loops and calculating rank");
    let mut discovered = vec![];
    for initial_sid in work_set.iter() {
        let mut stack = vec![];
        let initial_rank = blocks[initial_sid].get_rank();
        stack.push((initial_sid.clone(), initial_rank));
        while !stack.is_empty() {
            let (sid, parent_rank) = stack.pop().unwrap();
            if discovered.contains(&sid) {
                log::trace!("Loop detected - not updating rank");
            } else {
                let mut current_rank = blocks[&sid].get_rank();
                if current_rank == u32::MAX {
                    current_rank = parent_rank + 1;
                } else {
                    current_rank = std::cmp::max(parent_rank + 1, current_rank);
                }
                blocks.get_mut(&sid).unwrap().set_rank(current_rank);
                for (_index, port) in blocks[&sid].out_ports.iter() {
                    for peer in port.peers.as_ref().unwrap().iter() {
                        let new_sid = peer.sid.clone();
                        stack.push((new_sid, current_rank));
                    }
                }
                discovered.push(sid);
            }
        }
    }

    log::info!("Sorting by rank");
    for block in blocks.into_values() {
        result.push(block);
    }
    result.sort_by_key(|a| a.get_rank());

    Ok(result)
}

fn get_peer_data_type(
    blocks: &[Block],
    peer_sid: &crate::blocks::Sid,
    peer_port: u32,
    is_in_port: bool,
) -> SimulinkDataType {
    let mut result = SimulinkDataType::Unknown;
    for block in blocks.iter() {
        if block.get_sid() == peer_sid {
            let port_option = match is_in_port {
                true => block.in_ports.get(&peer_port),
                false => block.out_ports.get(&peer_port),
            };
            if let Some(v) = port_option {
                result = v.data_type;
            }
        }
    }
    result
}

fn propagate_data_types(ordered_blocks: &mut Vec<Block>) -> bool {
    log::trace!("Propagating data types");
    let mut work_performed = false;
    for i in 0..ordered_blocks.len() {
        for p in 1..(ordered_blocks[i].in_ports.len() + 1) as u32 {
            let peer_sid = ordered_blocks[i].in_ports[&p].peers.as_ref().unwrap()[0]
                .sid
                .clone();
            let peer_port = ordered_blocks[i].in_ports[&p].peers.as_ref().unwrap()[0].port_number;
            let peer_data_type = get_peer_data_type(ordered_blocks, &peer_sid, peer_port, false);
            match ordered_blocks[i].in_ports[&p].data_type {
                SimulinkDataType::Inherit | SimulinkDataType::Unknown => match peer_data_type {
                    SimulinkDataType::Inherit | SimulinkDataType::Unknown => {
                        //nothing done
                    }
                    v => {
                        work_performed = true;
                        ordered_blocks
                            .get_mut(i)
                            .unwrap()
                            .in_ports
                            .get_mut(&p)
                            .unwrap()
                            .data_type = v;
                    }
                },
                _ => {}
            }
        }
        for p in 1..(ordered_blocks[i].out_ports.len() + 1) as u32 {
            match ordered_blocks[i].out_ports[&p].data_type {
                SimulinkDataType::Inherit | SimulinkDataType::Unknown => {
                    for q in 0..ordered_blocks[i].out_ports[&p]
                        .peers
                        .as_ref()
                        .unwrap()
                        .len()
                    {
                        let peer_sid = ordered_blocks[i].out_ports[&p].peers.as_ref().unwrap()[q]
                            .sid
                            .clone();
                        let peer_port =
                            ordered_blocks[i].out_ports[&p].peers.as_ref().unwrap()[q].port_number;
                        let peer_data_type =
                            get_peer_data_type(ordered_blocks, &peer_sid, peer_port, true);
                        match peer_data_type {
                            SimulinkDataType::Inherit | SimulinkDataType::Unknown => {
                                //nothing to do
                            }
                            v => {
                                work_performed = true;
                                ordered_blocks
                                    .get_mut(i)
                                    .unwrap()
                                    .in_ports
                                    .get_mut(&p)
                                    .unwrap()
                                    .data_type = v;
                            }
                        }
                    }
                }
                _ => {}
            }
        }
    }
    work_performed
}

pub fn check_blocks(ordered_blocks: &mut Vec<Block>) -> Option<String> {
    let mut result = String::new();

    log::info!("Performing data type propagation");
    loop {
        if !propagate_data_types(ordered_blocks) {
            break;
        }
        log::trace!("Updating block data types");
        for i in 0..ordered_blocks.len() {
            ordered_blocks.get_mut(i).unwrap().update_data_types();
        }
    }

    log::info!("Performing type consistency checks");
    for i in 0..ordered_blocks.len() {
        for p in 1..(ordered_blocks[i].in_ports.len() + 1) as u32 {
            let peer_sid = ordered_blocks[i].in_ports[&p].peers.as_ref().unwrap()[0]
                .sid
                .clone();
            let peer_port = ordered_blocks[i].in_ports[&p].peers.as_ref().unwrap()[0].port_number;
            let peer_data_type = get_peer_data_type(ordered_blocks, &peer_sid, peer_port, false);
            let data_type = ordered_blocks[i].in_ports[&p].data_type;
            if peer_data_type != data_type {
                log::error!(
                    "Inconsistent data types on link from {} to {} : {} <> {}",
                    peer_sid.sid,
                    ordered_blocks[i].get_sid().sid,
                    peer_data_type.as_rust_type_string(),
                    data_type.as_rust_type_string()
                );
            }
        }
        for p in 1..(ordered_blocks[i].out_ports.len() + 1) as u32 {
            for q in 0..ordered_blocks[i].out_ports[&p]
                .peers
                .as_ref()
                .unwrap()
                .len()
            {
                let peer_sid = ordered_blocks[i].out_ports[&p].peers.as_ref().unwrap()[q]
                    .sid
                    .clone();
                let peer_port =
                    ordered_blocks[i].out_ports[&p].peers.as_ref().unwrap()[q].port_number;
                let peer_data_type = get_peer_data_type(ordered_blocks, &peer_sid, peer_port, true);
                let data_type = ordered_blocks[i].out_ports[&p].data_type;
                if peer_data_type != data_type {
                    log::error!(
                        "Inconsistent data types on link from {} to {} : {} <> {}",
                        ordered_blocks[i].get_sid().sid,
                        peer_sid.sid,
                        data_type.as_rust_type_string(),
                        peer_data_type.as_rust_type_string()
                    );
                }
            }
        }
    }

    log::info!("Performing block self checks");
    for block in ordered_blocks.iter() {
        match block.specialization.check_integrity(block) {
            Ok(_) => {}
            Err(e) => result += &e,
        }
    }
    if result.is_empty() {
        return None;
    }
    Some(result)
}

fn extract_var_from_line(line: &str) -> Option<(&str, &str, &str)> {
    let index = line.find("{{")?;
    let end_index = line.find("}}")?;
    let head = &line[0..index];
    let variable = &line[index + 2..end_index];
    let tail = &line[end_index + 2..];

    Some((head, variable, tail))
}

fn parse_template(
    filename: &str,
    values: &HashMap<&str, &String>,
) -> Result<String, std::io::Error> {
    let mut result = vec![];
    let file = std::io::BufReader::new(std::fs::File::open(filename)?);
    for line_result in file.lines() {
        let line = line_result?;
        if line.contains("{{") {
            let mut replaced = String::from("");
            replaced.reserve(line.len());
            let mut current = line.as_str();
            loop {
                match extract_var_from_line(current) {
                    Some((head, variable, tail)) => {
                        replaced.push_str(head);
                        replaced.push_str(values[&variable]);
                        current = tail;
                    }
                    None => {
                        replaced.push_str(current);
                        break;
                    }
                }
            }
            result.push(replaced);
        } else {
            result.push(line);
        }
    }
    result.push(String::from("")); //to append a linebreak
    Ok(result.join("\n"))
}

pub fn generate_code(
    name: String,
    version: String,
    ordered_blocks: Vec<Block>,
    cycle_time_us: u32,
    output_dir: &str,
    template_dir: &str,
) -> Result<bool, std::io::Error> {
    std::fs::create_dir_all(format!("./{}/src", output_dir).as_str())?;
    {
        let params = HashMap::from([("name", &name), ("version", &version)]);
        let cargo_toml_content = parse_template(
            format!("{}/Cargo.toml.jinja2", template_dir).as_str(),
            &params,
        )?;
        let mut output = std::fs::File::create(format!("./{}/Cargo.toml", output_dir).as_str())?;
        output.write_all(cargo_toml_content.as_bytes())?;
    }
    {
        let mut simulation_state_contents = String::new();
        let mut simulation_state_new_contents = String::new();
        let mut setup_contents = String::new();
        let mut step_contents = String::new();
        let mut teardown_contents = String::new();
        for block in ordered_blocks {
            if let Some(v) = block.generate_ports_storage_entry_code() {
                simulation_state_contents += v.as_str();
            }
            if let Some(v) = block.specialization.generate_state_entry_code(&block) {
                simulation_state_contents += v.as_str();
            }
            if let Some(v) = block.generate_ports_storage_init_code() {
                simulation_state_new_contents += v.as_str();
            }
            if let Some(v) = block.specialization.generate_state_init_code(&block) {
                simulation_state_new_contents += v.as_str();
            }
            if let Some(v) = block.specialization.generate_setup_code(&block) {
                setup_contents += v.as_str();
            }
            if let Some(v) = block.specialization.generate_update_code(&block) {
                step_contents += v.as_str();
            }
            if let Some(v) = block.specialization.generate_outputs_code(&block) {
                step_contents += v.as_str();
            }
            if let Some(v) = block.specialization.generate_teardown_code(&block) {
                teardown_contents += v.as_str();
            }
        }
        let mut params = HashMap::<&str, &String>::new();
        let empty = String::from("");
        let underscore = String::from("_");
        let cycle_time_string = format!("{}", cycle_time_us);
        params.insert("simulation_state", &simulation_state_contents);
        params.insert("simulation_state_new", &simulation_state_new_contents);
        params.insert("setup", &setup_contents);
        params.insert(
            "setup_state_prefix",
            match setup_contents.is_empty() {
                true => &underscore,
                false => &empty,
            },
        );
        params.insert("step", &step_contents);
        params.insert(
            "step_state_prefix",
            match step_contents.is_empty() {
                true => &underscore,
                false => &empty,
            },
        );
        params.insert("teardown", &teardown_contents);
        params.insert(
            "teardown_state_prefix",
            match teardown_contents.is_empty() {
                true => &underscore,
                false => &empty,
            },
        );
        params.insert("cycle_time_us", &cycle_time_string);
        let main_rs_content =
            parse_template(format!("{}/main.rs.jinja2", template_dir).as_str(), &params)?;
        let formatted: String = match rustfmt_wrapper::rustfmt(main_rs_content) {
            Ok(v) => v,
            Err(e) => {
                return Err(std::io::Error::new(std::io::ErrorKind::Other, e));
            }
        };
        let mut output = std::fs::File::create(format!("./{}/src/main.rs", output_dir).as_str())?;
        output.write_all(formatted.as_bytes())?;

        Ok(true)
    }
}
