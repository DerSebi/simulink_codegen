use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::collections::HashSet;
use std::io::Read;
use std::str::FromStr;

mod blocks;
mod codegen;

#[derive(Clone, PartialEq, Eq, Hash)]
struct PortConnection {
    sid: blocks::Sid,
    port_number: u32,
}

impl PortConnection {
    fn new(source: String) -> PortConnection {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^(.*)#(in|out):(\d+)$").unwrap();
        }
        let captures = RE.captures(source.as_str()).unwrap();
        let sid = blocks::Sid::parse(&captures[1]).unwrap();
        let port_number: u32 = captures[3].parse().unwrap();
        PortConnection { sid, port_number }
    }

    fn from_line(sid: blocks::Sid, port_number: u32) -> PortConnection {
        PortConnection { sid, port_number }
    }
}

pub struct Line {
    src: PortConnection,
    dst: Vec<PortConnection>,
}

impl Line {
    fn new(mut parameters: Vec<(String, String)>) -> Line {
        let ignored_parameters = vec!["Points", "ZOrder", "Name", "Labels"];
        parameters.retain(|a| !ignored_parameters.contains(&a.0.as_str()));
        let mut src = None;
        let mut dst = vec![];
        for param in parameters {
            match param.0.as_str() {
                "Src" => {
                    src = Some(PortConnection::new(param.1));
                }
                "Dst" => {
                    dst.push(PortConnection::new(param.1));
                }
                v => {
                    log::warn!("Unsupported parameter for <Line>: {}", v);
                }
            };
        }
        Line {
            src: src.unwrap(),
            dst,
        }
    }

    fn from_goto(goto_connection: &GotoConnection) -> Line {
        let src = PortConnection::from_line(goto_connection.goto_sid.clone(), 1);
        let mut dst = vec![];
        for from in goto_connection.from_sids.iter() {
            dst.push(PortConnection::from_line(from.clone(), 1));
        }
        Line { src, dst }
    }
}

pub struct Port {
    storage_location: String,
    data_type: blocks::SimulinkDataType,
    peers: Option<Vec<PortConnection>>,
}

impl Port {
    fn new(data_type: blocks::SimulinkDataType) -> Port {
        Port {
            storage_location: String::new(),
            data_type,
            peers: None,
        }
    }
}

fn archive_to_string(
    archive: &mut zip::ZipArchive<std::fs::File>,
    filename: &str,
) -> Option<String> {
    let mut file = match archive.by_name(filename) {
        Ok(file) => file,
        Err(..) => {
            log::error!("File {} file not found", filename);
            return None;
        }
    };
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    Some(contents)
}

fn element_from_xml_path<'a>(
    doc: &'a roxmltree::Document<'a>,
    path: Vec<&str>,
) -> Option<roxmltree::Node<'a, 'a>> {
    let root = doc.root();
    let mut current_node = root;
    for element in path.iter() {
        let mut found = false;
        for child in current_node.children() {
            if child.is_element() && child.tag_name().name() == *element {
                current_node = child;
                found = true;
                break;
            }
        }
        if !found {
            return None;
        }
    }
    Some(current_node)
}

struct GotoConnection {
    goto_sid: blocks::Sid,
    from_sids: Vec<blocks::Sid>,
}

struct ModelParseContext {
    blocks: HashMap<blocks::Sid, blocks::Block>,
    lines: Vec<Line>,
    reconnect_inports: HashMap<PortConnection, blocks::Sid>,
    reconnect_outports: HashMap<PortConnection, blocks::Sid>,
    unsupported_blocks: HashSet<String>,
    goto_registry: HashMap<String, GotoConnection>,
}

impl ModelParseContext {
    fn new() -> ModelParseContext {
        let lines: Vec<Line> = vec![];
        let blocks = HashMap::new();
        let reconnect_inports = HashMap::new();
        let reconnect_outports = HashMap::new();
        let unsupported_blocks = HashSet::new();
        let goto_registry = HashMap::new();

        ModelParseContext {
            blocks,
            lines,
            reconnect_inports,
            reconnect_outports,
            unsupported_blocks,
            goto_registry,
        }
    }
}

fn parse_system(
    system: &roxmltree::Node,
    context: &mut ModelParseContext,
    parent_sid: blocks::Sid,
    path_in_model: Vec<String>,
) -> bool {
    let mut unknown_blocks_present = false;
    for elem in system.children() {
        if elem.is_element() {
            if elem.has_tag_name("Block") {
                let name = elem.attribute("Name").unwrap();
                let block_type = elem.attribute("BlockType").unwrap();
                let sid = blocks::Sid::parse(elem.attribute("SID").unwrap()).unwrap();
                let mut parameters = HashMap::new();
                for child in elem.children() {
                    if child.has_tag_name("P") {
                        let key = child.attribute("Name").unwrap().to_string();
                        let value = child.text().unwrap_or("").to_string();
                        parameters.insert(key, value);
                    }
                }
                if block_type == "SubSystem" {
                    for child in elem.children() {
                        if child.has_tag_name("System") {
                            let mut path_in_model_new = path_in_model.clone();
                            path_in_model_new.push(name.to_string());
                            unknown_blocks_present |=
                                parse_system(&child, context, sid, path_in_model_new);
                            break;
                        }
                    }
                    continue;
                } else if block_type == "Inport" && parent_sid.sid != 0 {
                    let port = match parameters.get("Port") {
                        Some(v) => v.parse::<u32>().unwrap(),
                        None => 1,
                    };
                    context.reconnect_inports.insert(
                        PortConnection::from_line(parent_sid.clone(), port),
                        sid.clone(),
                    );
                    parameters.insert(String::from("Direction"), String::from("In"));
                } else if block_type == "Outport" && parent_sid.sid != 0 {
                    let port = match parameters.get("Port") {
                        Some(v) => v.parse::<u32>().unwrap(),
                        None => 1,
                    };
                    context.reconnect_outports.insert(
                        PortConnection::from_line(parent_sid.clone(), port),
                        sid.clone(),
                    );
                    parameters.insert(String::from("Direction"), String::from("Out"));
                } else if block_type == "Goto" {
                    let goto_tag = parameters.get("GotoTag").unwrap();
                    match context.goto_registry.get_mut(goto_tag) {
                        Some(v) => {
                            v.goto_sid = sid.clone();
                        }
                        None => {
                            context.goto_registry.insert(
                                goto_tag.clone(),
                                GotoConnection {
                                    goto_sid: sid.clone(),
                                    from_sids: vec![],
                                },
                            );
                        }
                    }
                } else if block_type == "From" {
                    let goto_tag = parameters.get("GotoTag").unwrap();
                    match context.goto_registry.get_mut(goto_tag) {
                        Some(v) => {
                            v.from_sids.push(sid.clone());
                        }
                        None => {
                            context.goto_registry.insert(
                                goto_tag.clone(),
                                GotoConnection {
                                    goto_sid: blocks::Sid::new(0, 0),
                                    from_sids: vec![sid.clone()],
                                },
                            );
                        }
                    }
                }
                match blocks::block_factory(
                    name.to_string(),
                    sid.clone(),
                    block_type.to_string(),
                    parameters,
                    path_in_model.clone(),
                    &mut context.unsupported_blocks,
                ) {
                    Some(b) => {
                        context.blocks.insert(sid, b);
                    }
                    None => {
                        unknown_blocks_present = true;
                    }
                }
            } else if elem.has_tag_name("Line") {
                let mut parameters: Vec<(String, String)> = vec![];
                parse_line(&elem, &mut parameters);
                context.lines.push(Line::new(parameters));
            }
        }
    }
    unknown_blocks_present
}

fn parse_line(elem: &roxmltree::Node, parameters: &mut Vec<(String, String)>) {
    for child in elem.children() {
        if !child.is_element() {
            continue;
        }
        if child.has_tag_name("P") {
            let key = child.attribute("Name").unwrap().to_string();
            let value = child.text().unwrap().to_string();
            parameters.push((key, value));
        } else if child.has_tag_name("Branch") {
            parse_line(&child, parameters);
        } else {
            log::warn!("Unsupported tag below <Line>: {:?}", child.tag_name());
        }
    }
}

fn main() {
    let args = clap::App::new("Simulink Code Generator")
        .version("1.0")
        .author("Sebastian Schlingmann")
        .about("Generate Rust code from Simulink model.")
        .arg(
            clap::Arg::with_name("model")
                .short("m")
                .long("model")
                .help("Input Simulink model")
                .takes_value(true)
                .required(true),
        )
        .arg(
            clap::Arg::with_name("output")
                .short("o")
                .long("output")
                .help("Output directory")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("cycle_time")
                .short("c")
                .long("cycle-time")
                .help("Time for one cycle in microseconds")
                .takes_value(true)
                .required(true),
        )
        .arg(
            clap::Arg::with_name("template_directory")
                .short("t")
                .long("template-directory")
                .help("Directory for Code generation templates")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("verbosity")
                .short("v")
                .long("verbosity")
                .help("Verbosity of the output (Error, Warn, Info, Debug, Trace)")
                .takes_value(true),
        )
        .get_matches();

    let model_name = args.value_of("model").unwrap();
    let cycle_time_us = args.value_of("cycle_time").unwrap().parse::<u32>().unwrap();
    let output_dir = args.value_of("output").unwrap_or("output");
    let template_dir = args.value_of("template_directory").unwrap_or("templates");
    let verbosity = match log::Level::from_str(args.value_of("verbosity").unwrap_or("Info")) {
        Err(e) => {
            println!("Invalid value for verbosity specified: {}", e);
            std::process::exit(1);
        }
        Ok(v) => v.to_level_filter(),
    };

    let mut log_builder = env_logger::Builder::new();

    log_builder
        .format_timestamp_millis()
        .filter_level(verbosity)
        .init();
    let fname = std::path::Path::new(model_name);
    let model_basename = fname
        .file_stem()
        .unwrap()
        .to_os_string()
        .into_string()
        .unwrap();

    log::info!("Opening model archive");
    let zipfile = std::fs::File::open(&fname).unwrap();
    let mut archive = zip::ZipArchive::new(zipfile).unwrap();
    let contents = match archive_to_string(&mut archive, "simulink/blockdiagram.xml") {
        Some(v) => v,
        None => std::process::exit(2),
    };
    let metadata = match archive_to_string(&mut archive, "metadata/coreProperties.xml") {
        Some(v) => v,
        None => std::process::exit(2),
    };

    log::info!("Parsing model XML");
    let doc = roxmltree::Document::parse(contents.as_str()).unwrap();
    let system = match element_from_xml_path(&doc, vec!["ModelInformation", "Model", "System"]) {
        None => {
            log::error!("Invalid XML - no System tag found");
            std::process::exit(1);
        }
        Some(v) => v,
    };
    let mut parse_context = ModelParseContext::new();
    let unknown_blocks_present = parse_system(
        &system,
        &mut parse_context,
        blocks::Sid::new(0, 0),
        vec![model_basename],
    );
    if unknown_blocks_present {
        log::error!("Unknown blocks found - aborting code generation");
        log::error!(
            "{} missing block implementations: ",
            parse_context.unsupported_blocks.len()
        );
        let mut blocks: Vec<String> = parse_context.unsupported_blocks.into_iter().collect();
        blocks.sort();
        for block in blocks {
            log::error!("\t {}", block);
        }
        std::process::exit(1);
    }
    // reconnect in and out ports for subsystems
    for line in parse_context.lines.iter_mut() {
        if parse_context.reconnect_outports.contains_key(&line.src) {
            line.src =
                PortConnection::from_line(parse_context.reconnect_outports[&line.src].clone(), 1);
        }
        for dst in line.dst.iter_mut() {
            if parse_context.reconnect_inports.contains_key(dst) {
                *dst = PortConnection::from_line(parse_context.reconnect_inports[dst].clone(), 1);
            }
        }
    }
    // create additional lines for goto connections
    for (_, connection) in parse_context.goto_registry {
        parse_context.lines.push(Line::from_goto(&connection));
    }

    let meta_doc = roxmltree::Document::parse(metadata.as_str()).unwrap();
    let mut version = "1.0.0".to_string();
    for elem in meta_doc.descendants() {
        if elem.has_tag_name("coreProperties") {
            for child in elem.children() {
                if child.has_tag_name("revision") {
                    version = format!("{}.0", child.text().unwrap());
                }
            }
        }
    }

    let name = fname.file_stem().unwrap().to_str().unwrap().to_string();

    log::info!("Ordering blocks");
    let mut ordered_blocks = match codegen::order_blocks(parse_context.lines, parse_context.blocks)
    {
        Ok(v) => v,
        Err(e) => {
            log::error!("Failure during ordering stage: {}", e);
            std::process::exit(3);
        }
    };
    log::info!("Checking blocks");
    if let Some(v) = codegen::check_blocks(&mut ordered_blocks) {
        log::error!("Block check failure: {}", v);
        std::process::exit(3);
    }
    log::info!("Generating code");
    match codegen::generate_code(
        name,
        version,
        ordered_blocks,
        cycle_time_us,
        output_dir,
        template_dir,
    ) {
        Ok(_) => {
            log::info!("Done");
        }
        Err(e) => {
            log::error!("Error during code generation: {}", e);
        }
    }

    std::process::exit(0);
}
