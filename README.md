## Name
Simulink Codegen

## Description
This code aims to generate standalone Rust code from a Simulink .slx model.
It is a toy project and does not aim for complete coverage of all features.

Currently supported features:
 * Basic code generation
 * Subsystems
 * Block types:
   * Constant
   * Delay
   * Display
   * From
   * Goto
   * Ground
   * Inport
   * Outport
   * RelationalOperator
   * Stop
   * Sum
   * Terminator

The implementation is not aiming for efficiency at the moment and is also using a lot of unwrap() calls without proper error handling.
A lot of the code generation stages could be combined eventually.
Another caveat is, that the code currently only supports the functionality of the implemented blocks that are necessary for the test models.
Functionality on all fronts can be improved.

## Usage
You have to provide parameters for cycle time and for the model to be converted.
e.g.
> cargo run -- --cycle-time 10000 --model model/my_model.slx

Output will be generated in the outputs folder and can be run with the cargo run command.

## Roadmap
I don't have a fixed plan for this, but I'm planning to add the following things:
 * Basic S-function support
 * Support for Busses
 * Support for referenced models
 * Support for Library Links

## Contributing
If you think this is useful or plan on playing with it for a bit I am happy to take pull requests.
It's fairly easy to add new block types.

## License
GPL-3.0

## Project status
This is just a toy for the time being. I'm not planning on making it a complete implementation.
